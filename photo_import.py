#!/usr/bin/env python

# Imports photos like fspot

import sys
import os
import pyexiv2
import time
import shutil
from datetime import datetime

import_dir = ""
output_dir = ""

#DATE_FORMAT   = '%Y%m%d_%H%M%S'
allowed_files = ['.jpg', '.jpeg', '.raw', '.cr2', '.nef']


def is_a_photo(path):
    """
    We only want to rename the files right?
    Return True or False depending on whether object is a file or a directory
    """

    if os.path.isdir(os.path.join(path,file)):
        return None

    (dir,filename) = os.path.split(path)
    (name,ext) = os.path.splitext(filename)
 
    if not allowed_files.count(ext.lower()):
        print "Not allowed filetype"
        return None

    image = None
    metadata = None

    #try:
    #    print path
    #    image = pyexiv2.Image(path)
    #except Exception as e:
    #    print e
    #    return None
  
    try:
        print path
        tag = pyexiv2.ImageMetadata(path)
        print tag
        #metadata = tag.read()
        #print metadata.exif_keys
        #print metadata
        #print metadata['Exif.Image.DateTime']
        tag.read()
        metadata = tag['Exif.Photo.DateTimeOriginal']
        # 2011:08:07 16:26:15
        metadata = datetime.strptime(metadata.human_value, "%Y:%m:%d %H:%M:%S")
        #datetime.date(year, month, day)
        #image.readMetadata()
        #metadata = image['Exif.Photo.DateTimeOriginal']
    except Exception as e:
        print e
        return None
    finally:
        if image != None:        
            del image

    return metadata


def increment_filename(filename):
    
    (name,ext) = os.path.splitext(filename)
    parts = filename.split('_fscopy')
  
    if len(parts) > 1:
        copy_number = int(parts[1][0]) + 1   
        filename = parts[0] + "_fscopy" + str(copy_number) + ext
    else:
        filename = name + "_fscopy1" + ext          

    return filename


def copy_file(filepath, output_dir, creation_date):

    # Get year month and day from exif data  
    (dir,filename) = os.path.split(filepath)
    (name,ext) = os.path.splitext(filename)

    date_filepath = datetime.strftime(creation_date, "%Y/%m/%d")

    output_directory = os.path.join(output_dir, date_filepath)

    if not os.path.isdir(output_directory):
        os.makedirs(output_directory)
 
    output_filepath = os.path.join(output_directory, filename)

    while os.path.exists(output_filepath):
        filename = increment_filename(filename)
        output_filepath = os.path.join(output_directory, filename)
     
    shutil.copy(filepath, output_filepath)
    print output_filepath, creation_date


if __name__ == '__main__':

    import_dir = sys.argv[1];
    output_dir = sys.argv[2];

    print "Searching ", import_dir

    if os.path.isdir(import_dir):
        """Recursively walk the directory listed as Arg 1 and work each file."""
        print
        for path,dirs,files in os.walk(import_dir):  

            if path != output_dir:
                for file in files:
                    filepath = os.path.join(path,file)
                    metadata = is_a_photo(filepath)             
                    if metadata != None:
                        print metadata            
                        copy_file(filepath, output_dir, metadata)
                    else:
                        NoMetaDataDir = output_dir + 'NoMetaData/'
                        if not os.path.isdir(NoMetaDataDir):
                            os.makedirs(NoMetaDataDir)
                        shutil.copy(filepath, NoMetaDataDir + os.path.split(filepath)[1])
    else:
        print "\nNo path to rename specified.\n"
        print "Usage:\n\tpython", import_dir, "<path to directory of source files>"
        sys.exit(1)


